﻿using AutoMapper;
using MovieAPI.Models;

namespace MovieAPI.AutoMapper
{
    /// <summary>
    /// AutoMapper definitions for DTO's related to the Movie model.
    /// </summary>
    public class MovieMapping : Profile
    {
        public MovieMapping()
        {
            CreateMap<MovieDtoGet, Movie>().ReverseMap();
            CreateMap<MovieDtoCreate, Movie>().ReverseMap();
            CreateMap<MovieDtoUpdate, Movie>().ReverseMap();
        }
    }
}

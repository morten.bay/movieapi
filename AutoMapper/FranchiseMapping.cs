﻿using AutoMapper;
using MovieAPI.Models;

namespace MovieAPI.AutoMapper
{
    /// <summary>
    /// AutoMapper definitions for DTO's related to the Franchise model.
    /// </summary>
    public class FranchiseMapping : Profile
    {
        public FranchiseMapping()
        {
            CreateMap<FranchiseDtoGet, Franchise>().ReverseMap();
            CreateMap<FranchiseDtoCreate, Franchise>().ReverseMap();
            CreateMap<FranchiseDtoUpdate, Franchise>().ReverseMap();
        }
    }
}

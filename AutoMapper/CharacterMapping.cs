﻿using AutoMapper;
using MovieAPI.Models;

namespace MovieAPI.AutoMapper
{
    /// <summary>
    /// AutoMapper definitions for DTO's related to the Character model.
    /// </summary>
    public class CharacterMapping : Profile
    {
        public CharacterMapping()
        {
            CreateMap<CharacterDtoGet, Character>().ReverseMap();
            CreateMap<CharacterDtoCreate, Character>().ReverseMap();
            CreateMap<CharacterDtoUpdate, Character>().ReverseMap();
        }
    }
}

﻿using MovieAPI.Models;

namespace MovieAPI.Interfaces
{

    /// <summary>
    /// Interface for the FranchiseRepository
    /// </summary>
    public interface IFranchiseRepository
    {
        /// <summary>
        /// Create a new Franchise.
        /// </summary>
        /// <param name="franchise">A Franchise object</param>
        /// <returns>A Franchise object of the new franchise entity.</returns>
        Task<Franchise> CreateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Get all Franchises.
        /// </summary>
        /// <returns>A list of all Franchises.</returns>
        Task<IEnumerable<Franchise>> GetFranchisesAsync();

        /// <summary>
        /// Get a Franchise by id.
        /// </summary>
        /// <param name="id">The id of the Franchise to get.</param>
        /// <returns>A Franchise object.</returns>
        Task<Franchise> GetFranchiseByIdAsync(int id);

        /// <summary>
        /// Get all movies in a Franchise by id.
        /// </summary>
        /// <param name="id">The id of the Franchise</param>
        /// <returns>a List of Movie objects.</returns>
        Task<IEnumerable<Movie>> GetFranchiseMoviesByIdAsync(int id);

        /// <summary>
        /// Get all Characters in a Franchise by id.
        /// </summary>
        /// <param name="id">The id of the Franchise</param>
        /// <returns>A List of Character objects.</returns>
        Task<IEnumerable<Character>> GetFranchiseCharactersByIdAsync(int id);

        /// <summary>
        /// Update a Franchise by id. Id is passed in the Franchise object.
        /// </summary>
        /// <param name="franchise">A Franchise object with the updated values</param>
        /// <returns>A default Task object.</returns>
        Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Update movies in a Franchise.
        /// </summary>
        /// <param name="franchiseId">The id of the Franchise</param>
        /// <param name="movieIds">A integer array of character id's.</param>
        /// <returns>A default Task object.</returns>
        Task UpdateFranchiseMoviesAsync(int franchiseId, int[] movieIds);

        /// <summary>
        /// Delete a Franchise by id.
        /// </summary>
        /// <param name="id">The id of the Franchise to delete.</param>
        /// <returns>A Franchise object of the deleted entity.</returns>
        Task<Franchise> DeleteFranchiseAsync(int id);
    }
}

﻿using MovieAPI.Models;

namespace MovieAPI.Interfaces
{

    /// <summary>
    /// Interface for the MovieRepository
    /// </summary>
    public interface IMovieRepository
    {
        /// <summary>
        /// Create a new Movie.
        /// </summary>
        /// <param name="movie">A Movie object.</param>
        /// <returns>A Movie object of the new movie entity.</returns>
        Task<Movie> CreateMovieAsync(Movie movie);

        /// <summary>
        /// Get all Movies.
        /// </summary>
        /// <returns>A list of all Movies.</returns>
        Task<IEnumerable<Movie>> GetMoviesAsync();

        /// <summary>
        /// Get a Movie by id.
        /// </summary>
        /// <param name="id">The id of the Movie to get.</param>
        /// <returns>A Movie object.</returns>
        Task<Movie> GetMovieByIdAsync(int id);

        /// <summary>
        /// Get all Characters in a movie by id.
        /// </summary>
        /// <param name="id">The id of the Movie.</param>
        /// <returns>A list of Character objects.</returns>
        Task<IEnumerable<Character>> GetMovieCharactersByIdAsync(int id);

        /// <summary>
        /// Update a Movie by id. Id is passed in the Movie object.
        /// </summary>
        /// <param name="movie">A Movie object with the updated values</param>
        /// <returns>A default Task object.</returns>
        Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Update Characters in a movie.
        /// </summary>
        /// <param name="movieId">The id of the Movie.</param>
        /// <param name="characterIds">A integer array of character id's.</param>
        /// <returns>A default Task object.</returns>
        Task UpdateMovieCharactersAsync(int movieId, int[] characterIds);

        /// <summary>
        /// Delete a Movie by id.
        /// </summary>
        /// <param name="id">The id of the Movie to delete.</param>
        /// <returns>A Movie object of the deleted entity.</returns>
        Task<Movie> DeleteMovieAsync(int id);
           
    }
}

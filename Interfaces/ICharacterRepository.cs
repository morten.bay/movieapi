﻿using MovieAPI.Models;

namespace MovieAPI.Interfaces
{
    /// <summary>
    /// Interface for the CharacterRepository
    /// </summary>
    public interface ICharacterRepository
    {
        /// <summary>
        /// Create a new Character.
        /// </summary>
        /// <param name="character">A Character object.</param>
        /// <returns>A Character object of the new Character entity.</returns>
        Task<Character> CreateCharacterAsync(Character character);

        /// <summary>
        /// Get all Characters.
        /// </summary>
        /// <returns>A List of all Characters.</returns>
        Task<IEnumerable<Character>> GetCharactersAsync();

        /// <summary>
        /// Get a Character by id.
        /// </summary>
        /// <param name="id">The id of the Character to get</param>
        /// <returns>A Character object</returns>
        Task<Character> GetCharacterByIdAsync(int id);

        /// <summary>
        /// Update a Character by id. Id is passed in the Character object.
        /// </summary>
        /// <param name="character">A Character object with the updated values</param>
        /// <returns>A default Task object</returns>
        Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Delete a Character by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A Character object of the deleted entity.</returns>
        Task<Character> DeleteCharacterAsync(int id);
    }
}

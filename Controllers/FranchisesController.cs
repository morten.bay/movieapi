﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Controllers
{
    /// <summary>
    /// The controller for the Franchises API endpoints.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseRepository _franchises;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseRepository franchises, IMapper mapper)
        {
            _franchises = franchises;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <returns>A json object with all the franchises.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDtoGet>>> GetAllFranchises()
        {
            try
            {
                var franchises = await _franchises.GetFranchisesAsync();
                if (!franchises.Any())
                {
                    return NotFound();
                }
                var franchisesDto = _mapper.Map<IEnumerable<FranchiseDtoGet>>(franchises);
                return Ok(franchisesDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get a franchise by id.
        /// </summary>
        /// <returns>A json object with the franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDtoGet>> GetFranchiseById(int id)
        {
            try
            {
                var franchise = await _franchises.GetFranchiseByIdAsync(id);
                if (franchise == null)
                {
                    return NotFound();
                }

                var franchiseDto = _mapper.Map<FranchiseDtoGet>(franchise);
                return Ok(franchiseDto);
            }
            
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        

        // PUT: api/Franchises/5
        /// <summary>
        /// Update a Franchise by id.
        /// </summary>
        /// <returns>200 if succeeded</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDtoUpdate franchiseDto)
        {
            try
            {
                var franchise = _mapper.Map<Franchise>(franchiseDto);
                franchise.Id = id;
                await _franchises.UpdateFranchiseAsync(franchise);
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // POST: api/Franchises
        /// <summary>
        /// Add a Franchise.
        /// </summary>
        /// <returns>A json object of the added Franchise.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<FranchiseDtoGet>> PostFranchise(FranchiseDtoCreate franchiseDto)
        {
            try
            {
                var franchise = _mapper.Map<Franchise>(franchiseDto);
                var createdFranchise = await _franchises.CreateFranchiseAsync(franchise);
                if (createdFranchise == null)
                {
                    return BadRequest();
                }

                var createdFranchiseDto = _mapper.Map<FranchiseDtoGet>(createdFranchise);
                return Ok(createdFranchiseDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a Franchise by id.
        /// </summary>
        /// <returns>200 if succeeded in deleting a Franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            try
            {
                var deletedfranchise = await _franchises.DeleteFranchiseAsync(id);
                if (deletedfranchise == null)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/Franchises/movies/5
        /// <summary>
        /// Get all Movies in a Franchise by id.
        /// </summary>
        /// <returns>A json object with the franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("Movies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieDtoGet>>> GetFranchiseMovies(int id)
        {
            try
            {
                var movies = await _franchises.GetFranchiseMoviesByIdAsync(id);
                if (!movies.Any())
                {
                    return NotFound();
                }

                var moviesDto = _mapper.Map<IEnumerable<MovieDtoGet>>(movies);
                return Ok(moviesDto);
            }

            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/Franchises/characters/5
        /// <summary>
        /// Get all Characters in a Franchise by id.
        /// </summary>
        /// <returns>A json object with the franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("Characters/{id}")]
        public async Task<ActionResult<IEnumerable<MovieDtoGet>>> GetFranchiseCharacters(int id)
        {
            try
            {
                var characters = await _franchises.GetFranchiseCharactersByIdAsync(id);
                if (!characters.Any())
                {
                    return NotFound();
                }

                var charactersDto = _mapper.Map<IEnumerable<CharacterDtoGet>>(characters);
                return Ok(charactersDto);
            }

            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // PUT: api/Franchises/Movies/5
        /// <summary>
        /// Update Movies in a Franchise by id.
        /// </summary>
        /// <returns>200 if succeeded in assigning the movies to the Franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("Movies/{id}")]
        public async Task<IActionResult> PutFranchiseMovies(int id, FranchiseDtoUpdateMovies UpdateMoviesDto)
        {
            try
            {
                await _franchises.UpdateFranchiseMoviesAsync(id, UpdateMoviesDto.Ids!);
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }
    }
}

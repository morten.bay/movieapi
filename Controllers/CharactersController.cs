﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Controllers
{
    /// <summary>
    /// The controller for the Characters API endpoints.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _characters;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterRepository characters, IMapper mapper)
        {
            _characters = characters;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Get all Characters.
        /// </summary>
        /// <returns>A json object with all characters</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDtoGet>>> GetAllCharacters()
        {
            try
            {
                var characters = await _characters.GetCharactersAsync();
                if (!characters.Any())
                {
                    return NotFound();
                }
                var charactersDto = _mapper.Map<IEnumerable<CharacterDtoGet>>(characters);
                return Ok(charactersDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/Characters/5
        /// <summary>
        /// Get a Character by id.
        /// </summary>
        /// <returns>A json object with the character</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDtoGet>> GetCharacterById(int id)
        {
            try
            {
                var character = await _characters.GetCharacterByIdAsync(id);
                if (character == null)
                {
                    return NotFound();
                }
                var characterDto = _mapper.Map<CharacterDtoGet>(character);
                return Ok(characterDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Update a Character by id.
        /// </summary>
        /// <returns>200 if succeeded</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterDtoUpdate characterDto)
        {
            try
            {
                var character = _mapper.Map<Character>(characterDto);
                character.Id = id;
                await _characters.UpdateCharacterAsync(character);
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // POST: api/Characters
        /// <summary>
        /// Add a Character.
        /// </summary>
        /// <returns>A json object of the added Character.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<CharacterDtoGet>> PostCharacter(CharacterDtoCreate characterDto)
        {
            try
            {
                var character = _mapper.Map<Character>(characterDto);
                var createdCharacter = await _characters.CreateCharacterAsync(character);
                if (createdCharacter == null)
                {
                    return BadRequest();
                }
                var createdCharacterDto = _mapper.Map<CharacterDtoGet>(createdCharacter);
                return Ok(createdCharacterDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete a Character by id.
        /// </summary>
        /// <returns>200 if succeeded in deleting the Character</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            try
            {
                var deletedCharacter = await _characters.DeleteCharacterAsync(id);
                if (deletedCharacter == null)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Controllers
{
    /// <summary>
    /// The controller for the Movies API endpoints.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movies;
        private readonly IMapper _mapper;

        public MoviesController(IMovieRepository movies, IMapper mapper)
        {
            _movies = movies;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns>A json object with all movies</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDtoGet>>> GetAllMovies()
        {
            try
            {
                var movies = await _movies.GetMoviesAsync();

                if (!movies.Any())
                {
                    return NotFound();
                }
                var moviesDto = _mapper.Map<IEnumerable<MovieDtoGet>>(movies);
                return Ok(moviesDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/Movies/5
        /// <summary>
        /// Get a movie by id.
        /// </summary>
        /// <returns>A json object with the movie</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDtoGet>> GetMovieById(int id)
        {
            try
            {
                var movie = await _movies.GetMovieByIdAsync(id);
                if (movie == null)
                {
                    return NotFound();
                }
                var movieDto = _mapper.Map<MovieDtoGet>(movie);
                return Ok(movieDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // GET: api/movie/characters/5
        /// <summary>
        /// Get all Characters in a Movie by id.
        /// </summary>
        /// <returns>A json object with the franchise</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("Characters/{id}")]
        public async Task<ActionResult<IEnumerable<MovieDtoGet>>> GetMovieCharacters(int id)
        {
            try
            {
                var characters = await _movies.GetMovieCharactersByIdAsync(id);
                if (!characters.Any())
                {
                    return NotFound();
                }

                var charactersDto = _mapper.Map<IEnumerable<CharacterDtoGet>>(characters);
                return Ok(charactersDto);
            }

            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // PUT: api/Movies/5
        /// <summary>
        /// Update a Movie by id.
        /// </summary>
        /// <returns>200 if succeeded</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieDtoUpdate movieDto)
        {
            try
            {
                var movie = _mapper.Map<Movie>(movieDto);
                movie.Id = id;
                await _movies.UpdateMovieAsync(movie);
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // POST: api/Movies
        /// <summary>
        /// Add a Movie.
        /// </summary>
        /// <returns>A json object of the added movie.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieDtoCreate movieDto)
        {
            try
            {
                var movie = _mapper.Map<Movie>(movieDto);
                var createdMovie = await _movies.CreateMovieAsync(movie);
                if (createdMovie == null)
                {
                    return BadRequest();
                }

                var createdMovieDto = _mapper.Map<MovieDtoGet>(createdMovie);
                return Ok(createdMovieDto);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete a Movie by id.
        /// </summary>
        /// <returns>200 if succeeded in deleting the movie</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            try
            {
                var deletedMovie = await _movies.DeleteMovieAsync(id);
                if (deletedMovie == null)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }

        // PUT: api/Movies/Characters/5
        /// <summary>
        /// Update characters in a movie by id.
        /// </summary>
        /// <returns>200 if succeeded in updating the characters in a movie</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("Characters/{id}")]
        public async Task<IActionResult> PutMovieCharacters(int id, MovieDtoUpdateCharacters UpdateCharactersDto)
        {
            try
            {
                await _movies.UpdateMovieCharactersAsync(id, UpdateCharactersDto.Ids!);
                return Ok();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                return Problem();
            }
        }
    }
}

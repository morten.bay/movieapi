﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Movie DTO object
    /// </summary>
    public class MovieDtoGet
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string TrailerUrl { get; set; }
    }
}

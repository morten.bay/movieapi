﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Franchise DTO object
    /// </summary>
    public class FranchiseDtoUpdateMovies
    {
        public int[]? Ids { get; set; }
    }
}

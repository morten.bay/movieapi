﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Movie DTO object
    /// </summary>
    public class MovieDtoUpdateCharacters
    {
        public int[]? Ids { get; set; }
    }
}

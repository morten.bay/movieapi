﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Character DTO object
    /// </summary>
    public class CharacterDtoGet
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? PictureURL { get; set; }
    }
}

﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Character DTO object
    /// </summary>
    public class CharacterDtoUpdate
    {
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? PictureURL { get; set; }
    }
}

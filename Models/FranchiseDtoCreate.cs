﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Franchise DTO object
    /// </summary>
    public class FranchiseDtoCreate
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}

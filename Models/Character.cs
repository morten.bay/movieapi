﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models
{
    /// <summary>
    /// The Model for the Character object.
    /// </summary>
    public class Character
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string Alias { get; set; }

        [MaxLength(50)]
        public string Gender { get; set; }

        [MaxLength(250)]
        public string PictureURL { get; set; }

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}

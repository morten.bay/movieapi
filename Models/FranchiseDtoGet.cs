﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Franchise DTO object
    /// </summary>
    public class FranchiseDtoGet
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}

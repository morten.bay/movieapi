﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Franchise DTO object
    /// </summary>
    public class FranchiseDtoUpdate
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
    }
}

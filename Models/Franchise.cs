﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models
{
    /// <summary>
    /// The Model for the Franchise object.
    /// </summary>
    public class Franchise
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string? Description { get; set; }

        // Relationships
        public ICollection<Movie>? Movies { get; set; }
    }
}

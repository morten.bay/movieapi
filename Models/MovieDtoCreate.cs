﻿namespace MovieAPI.Models
{
    /// <summary>
    /// Movie DTO object
    /// </summary>
    public class MovieDtoCreate
    {
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureUrl { get; set; }
        public string TrailerUrl { get; set; }
    }
}

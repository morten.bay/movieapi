﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models
{
    /// <summary>
    /// The Model for the Movie object.
    /// </summary>
    public class Movie
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }

        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }

        [Required]
        public int ReleaseYear { get; set; }

        [Required]
        [MaxLength(50)]
        public string Director { get; set; }

        [Required]
        [MaxLength(250)]
        public string PictureUrl { get; set; }

        [Required]
        [MaxLength(250)]
        public string TrailerUrl { get; set; }

        // Relationships
        public ICollection<Character>? Characters { get; set; }
        public Franchise? Franchise { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.DbContexts;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Repositories
{
    /// <summary>
    /// A class for the CharacterRepository containing Entity Framework operations.
    /// </summary>
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MovieAPIDbContext _ctx;
        public CharacterRepository(MovieAPIDbContext ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Character> CreateCharacterAsync(Character character)
        {
            _ctx.Character.Add(character);
            await _ctx.SaveChangesAsync();
            return character;
        }

        /// <inheritdoc/>
        public async Task<Character> GetCharacterByIdAsync(int id)
        {
            var character = await _ctx.Character.FirstOrDefaultAsync(character => character.Id == id);
            return character;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Character>> GetCharactersAsync()
        {
            return await _ctx.Character.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateCharacterAsync(Character character)
        {
            _ctx.Entry(character).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<Character> DeleteCharacterAsync(int id)
        {
            var character = await _ctx.Character.FindAsync(id);

            if (character != null)
            {
                _ctx.Character.Remove(character);
                await _ctx.SaveChangesAsync();
            }
            return character;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.DbContexts;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Repositories
{
    /// <summary>
    /// A class for the MovieRepository containing Entity Framework operations.
    /// </summary>
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieAPIDbContext _ctx;
        public MovieRepository(MovieAPIDbContext ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Movie> CreateMovieAsync(Movie movie)
        {
            _ctx.Movie.Add(movie);
            await _ctx.SaveChangesAsync();
            return movie;
        }

        /// <inheritdoc/>
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            var movie = await _ctx.Movie.FirstOrDefaultAsync(movie => movie.Id == id);

            return movie;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _ctx.Movie.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _ctx.Entry(movie).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<Movie> DeleteMovieAsync(int id)
        {
            var movie = await _ctx.Movie.FindAsync(id);

            if (movie != null)
            {
                _ctx.Movie.Remove(movie);
                await _ctx.SaveChangesAsync();
            }

            return movie;
        }

        /// <inheritdoc/>
        public async Task <IEnumerable<Character>> GetMovieCharactersByIdAsync(int id)
        {
            var movie = await _ctx.Movie.Include(movie => movie.Characters).SingleOrDefaultAsync(m => m.Id == id);
            var charactersList = new List<Character>();

            if (movie != null && movie.Characters != null)
            {
                foreach (var character in movie.Characters)
                {
                    charactersList.Add(character);
                }
            }

            return charactersList.Distinct();
        }

        /// <inheritdoc/>
        public async Task UpdateMovieCharactersAsync(int movieId, int[] characterIds)
        {
            var movie = _ctx.Movie.Include(movie => movie.Characters).Single(m => m.Id == movieId);
            var charactersToAdd = _ctx.Character.Where(e => characterIds.Contains(e.Id)).ToList();

            if (movie != null && charactersToAdd != null)
            {
                movie.Characters.Clear();
                foreach (var character in charactersToAdd)
                {
                    movie.Characters.Add(character);
                }
                await _ctx.SaveChangesAsync();
            }
        }
    }
}

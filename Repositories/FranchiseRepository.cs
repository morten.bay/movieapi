﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.DbContexts;
using MovieAPI.Interfaces;
using MovieAPI.Models;

namespace MovieAPI.Repositories
{
    /// <summary>
    /// A class for the FranchiseRepository containing Entity Framework operations.
    /// </summary>
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieAPIDbContext _ctx;
        public FranchiseRepository(MovieAPIDbContext ctx)
        {
            _ctx = ctx;
        }

        /// <inheritdoc/>
        public async Task<Franchise> CreateFranchiseAsync(Franchise franchise)
        {
            _ctx.Franchise.Add(franchise);
            await _ctx.SaveChangesAsync();
            return franchise;
        }

        /// <inheritdoc/>
        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            var franchise = await _ctx.Franchise.FirstOrDefaultAsync(franchise => franchise.Id == id);
            return franchise;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _ctx.Franchise.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _ctx.Entry(franchise).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<Franchise> DeleteFranchiseAsync(int id)
        {
            var franchise = await _ctx.Franchise.FindAsync(id);

            if (franchise != null)
            {
                _ctx.Franchise.Remove(franchise);
                await _ctx.SaveChangesAsync();
            }
            return franchise;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Movie>> GetFranchiseMoviesByIdAsync(int id)
        {
            var franchise = await _ctx.Franchise.Include(franchise => franchise.Movies).SingleOrDefaultAsync(f => f.Id == id);
            
            if(franchise != null && franchise.Movies != null)
            {
                return franchise.Movies;
            }
            else
            {
                return new List<Movie>(); // return empty object, if nothing is found.
            }
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Character>> GetFranchiseCharactersByIdAsync(int id)
        {
            var franchise = await _ctx.Franchise.Include(franchise => franchise.Movies).SingleOrDefaultAsync(f => f.Id == id);

            var charactersList = new List<Character>(); 
            if (franchise != null && franchise.Movies != null)
            {
                foreach(var movie in franchise.Movies)
                {
                    _ctx.Entry(movie).Collection(m => m.Characters).Load();
                    var charactersInMovie = movie.Characters.ToList();

                    foreach(var character in charactersInMovie)
                    {
                        charactersList.Add(character);
                    }
                }
            }
            return charactersList.Distinct();
        }

        /// <inheritdoc/>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, int[] movieIds)
        {
            var franchise = _ctx.Franchise.Single(f => f.Id == franchiseId);
            var moviesToAdd = _ctx.Movie.Where(m => movieIds.Contains(m.Id)).ToList();

            if (franchise != null && moviesToAdd != null)
            {
                foreach (var movie in moviesToAdd)
                {
                    _ctx.Entry(movie).Property("FranchiseId").CurrentValue = franchise.Id;
                }
                await _ctx.SaveChangesAsync();
            }
        }
    }
}

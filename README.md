# MovieAPI

A MovieAPI created with ASP.NET Core Web API in C#, featuring Entity Framework and Swagger / Open API.

A Database and Class Diagram is available in the "Docs" folder.

## Install

```
git clone git@gitlab.com:morten.bay/movieapi.git
```

## Usage

1. Edit the ConnectionStrings key in "appsettings.json" file to reflect your own SQL environment.

    example:

    ```
    "ConnectionStrings": { "MovieApiDb": "server=.; database=MovieApiDb; Trusted_connection=true;" }
    ```

2. Create the MovieApiDb database by running the following command in the PM console.

    ```
    update-database
    ```

3. Build and Run the Application in Visual Studio 2022. The Swagger / Open API webUI will start automatically.

## Features

The Web API features the following API Endpoints:

### Characters

```
GET /api/Characters - Get all Characters.
POST /api/Characters - Add a Character.
GET /api/Characters/{id} - Get a Character by id.
PUT /api/Characters/{id} - Update a Character by id.
DELETE /api/Characters/{id} - Delete a Character by id.
```

### Franchises

```
GET /api/Franchises - Get all franchises.
POST /api/Franchises - Add a Franchise.
GET /api/Franchises/{id} - Get a franchise by id.
PUT /api/Franchises/{id} - Update a Franchise by id.
DELETE /api/Franchises/{id} - Delete a Franchise by id.
GET /api/Franchises/Movies/{id} - Get all Movies in a Franchise by id.
PUT /api/Franchises/Movies/{id} - Update Movies in a Franchise by id.
GET /api/Franchises/Characters/{id} - Get all Characters in a Franchise by id.
```

### Movies

```
GET /api/Movies - Get all movies.
POST /api/Movies - Add a Movie.
GET /api/Movies/{id} - Get a movie by id.
PUT /api/Movies/{id} - Update a Movie by id.
DELETE /api/Movies/{id} - Delete a Movie by id.
GET /api/Movies/Characters/{id} - Get all Characters in a Movie by id.
PUT /api/Movies/Characters/{id} - Update characters in a movie by id.
```

## Contributors

-   [Alexander Kristensen (@alkrdev)](@alkrdev)
-   [Morten Bay Nielsen (@morten.bay)](@morten.bay)

## License

Noroff Accelerate, 2022.

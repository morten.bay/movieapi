﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;

namespace MovieAPI.DbContexts
{
    /// <summary>
    /// DbContext definition for the MovieAPI Database.
    /// </summary>
    public class MovieAPIDbContext : DbContext
    {

        public MovieAPIDbContext(DbContextOptions<MovieAPIDbContext> options) : base(options)
        {
            //If 'builder.Services.AddDbContext' is used, then also ensure that DbContext type accepts a DbContextOptions<TContext>
            //object in its constructor and passes it to the base constructor for DbContext.
        }

        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Add Franchises
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise { Id = 1, Name = "Marvel's Cinematic Universe", Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios." },
                new Franchise { Id = 2, Name = "Lord of the Rings", Description = "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil." },
                new Franchise { Id = 3, Name = "Star Wars", Description = "A space opera set - a long time ago in a galaxy far, far away." }
            );

            // Add Characters
            modelBuilder.Entity<Character>().HasData(
                new { Id = 1, FullName = "Tony Stark", Alias = "Iron Man", Gender = "Male", PictureURL = "http://" },
                new { Id = 2, FullName = "Frodo Baggins", Alias = "Frodo", Gender = "Male", PictureURL = "http://" },
                new { Id = 3, FullName = "Anakin Skywalker", Alias = "Darth Vader", Gender = "Male", PictureURL = "http://" }
            );

            // Add Movies
            modelBuilder.Entity<Movie>().HasData(
                new { Id = 4, FranchiseId = 1, MovieTitle = "Iron Man", Director = "Jon Favreau", Genre = "Action", ReleaseYear = 2008, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 5, FranchiseId = 1, MovieTitle = "Thor", Director = "Kenneth Branagh", Genre = "Action", ReleaseYear = 2011, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 6, FranchiseId = 1, MovieTitle = "Avengers: Age of Ultron", Director = "Joss Whedon", Genre = "Action", ReleaseYear = 2015, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 1, FranchiseId = 2, MovieTitle = "LOTR: The Fellowship of the Ring", Director = "Peter Jackson", Genre = "high fantasy", ReleaseYear = 2001, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 2, FranchiseId = 2, MovieTitle = "LOTR: The Two Towers", Director = "Peter Jackson", Genre = "high fantasy", ReleaseYear = 2002, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 3, FranchiseId = 2, MovieTitle = "LOTR: The Return of the King", Director = "Peter Jackson", Genre = "high fantasy", ReleaseYear = 2003, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 7, FranchiseId = 3, MovieTitle = "Episode IV – A New Hope", Director = "George Lucas", Genre = "Science Fiction", ReleaseYear = 1977, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 8, FranchiseId = 3, MovieTitle = "Episode V – The Empire Strikes Back", Director = "George Lucas", Genre = "Science Fiction", ReleaseYear = 1980, PictureUrl = "http://", TrailerUrl = "http://" },
                new { Id = 9, FranchiseId = 3, MovieTitle = "Episode VI – Return of the Jedi", Director = "George Lucas", Genre = "Science Fiction", ReleaseYear = 1983, PictureUrl = "http://", TrailerUrl = "http://" }
            );

            // Add Character to Movie Relationships
            modelBuilder.Entity<Movie>().HasMany(m => m.Characters).WithMany(m => m.Movies).UsingEntity(j => j.HasData(
                new { CharactersId = 1, MoviesId = 4 },
                new { CharactersId = 1, MoviesId = 6 },
                new { CharactersId = 2, MoviesId = 1 },
                new { CharactersId = 2, MoviesId = 2 },
                new { CharactersId = 2, MoviesId = 3 },
                new { CharactersId = 3, MoviesId = 7 },
                new { CharactersId = 3, MoviesId = 8 },
                new { CharactersId = 3, MoviesId = 9 }
            ));

        }
    }
}

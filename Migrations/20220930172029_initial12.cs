﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieAPI.Migrations
{
    public partial class initial12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 1,
                column: "Description",
                value: "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios.");

            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 2,
                column: "Description",
                value: "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil.");

            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 3,
                column: "Description",
                value: "A space opera set - a long time ago in a galaxy far, far away.");

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3,
                column: "MovieTitle",
                value: "LOTR: The Return of the King");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 1,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 2,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 3,
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3,
                column: "MovieTitle",
                value: "The Return of the King");
        }
    }
}
